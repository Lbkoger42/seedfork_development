Rails.application.routes.draw do
  get 'dashboard/create_donations'

  get 'dashboard/view_donations'

  get 'dashboard/order_entry'

  get 'dashboard/member'

  resources :addresses
  get 'dashboard/navigation_bar'

  get 'dashboard/about'

  get 'dashboard/contact'

  get 'dashboard/login'

  resources :donations
  root to: "dashboard#home"
  get 'dashboard/home'

  devise_for :users
  
  # Pages
#  get 'dashboard/about'
#  get 'dashboard/contact'
#  get 'dashboard/home'
#  get 'dashboard/login'
#  get 'dashboard/navigation_bar'
  
  get 'potato_controller/protocol1'

  get 'potato_controller/protocol2'

  get 'potato_controller/protocol3'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
