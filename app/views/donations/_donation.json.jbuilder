json.extract! donation, :id, :email, :produce, :quantity, :units, :data_entry_datetime, :pickup_date, :farmer_price, :picked_up, :dropped_off, :created_at, :updated_at
json.url donation_url(donation, format: :json)
