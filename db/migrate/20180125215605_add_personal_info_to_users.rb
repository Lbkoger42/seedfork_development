class AddPersonalInfoToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :first_name, :text
    add_column :users, :last_name, :text
    add_column :users, :street, :text
    add_column :users, :city, :text
    add_column :users, :state, :text
    add_column :users, :zip_code, :integer
    add_column :users, :phone, :text
  end
end
