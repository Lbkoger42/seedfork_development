class CreateDonations < ActiveRecord::Migration[5.1]
  def change
    create_table :donations do |t|
      t.text :email
      t.text :produce
      t.text :quantity
      t.text :units
      t.datetime :data_entry_datetime
      t.date :pickup_date
      t.decimal :farmer_price
      t.boolean :picked_up
      t.boolean :dropped_off

      t.timestamps
    end
  end
end
