class CreateUserInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :user_infos do |t|
      t.text :first_name
      t.text :last_name
      t.text :street
      t.text :city
      t.text :state
      t.integer :zip
      t.text :phone
      t.text :email

      t.timestamps
    end
  end
end
