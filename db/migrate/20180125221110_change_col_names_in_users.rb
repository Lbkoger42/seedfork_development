class ChangeColNamesInUsers < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :first_name, :text, null: false
    change_column :users, :last_name, :text, null: false
    change_column :users, :street, :text, null: false
    change_column :users, :city, :text, null: false
    change_column :users, :state, :text, null: false
    change_column :users, :zip_code, :integer, null: false
    change_column :users, :phone, :text, null: false
  end
end
