class CreateExamples < ActiveRecord::Migration[5.1]
  def change
    create_table :examples do |t|
      t.text :ex1
      t.integer :ex2

      t.timestamps
    end
  end
end
