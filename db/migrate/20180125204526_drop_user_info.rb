class DropUserInfo < ActiveRecord::Migration[5.1]
  def change
    drop_table :user_infos
  end
end
